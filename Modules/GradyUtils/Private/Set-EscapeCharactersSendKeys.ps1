function Set-EscapeCharactersSendKeys {
    Param(
        [parameter(Mandatory = $true, Position = 0)]
        [String]
        $string
    )

    $string = $string -replace '\)', '{)}'
    $string = $string -replace '\(', '{(}'
    $string = $string -replace '\+', '{+}'
    $string = $string -replace '\^', '{^}'
    $string = $string -replace '\%', '{%}'
    $string = $string -replace '\~', '{~}'

    return $string
}
