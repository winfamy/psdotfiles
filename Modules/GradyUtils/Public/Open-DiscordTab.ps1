Function Open-DiscordTab() {
    start microsoft-edge:https://safe.menlosecurity.com/https://discord.com/login
    $wshell = New-Object -ComObject wscript.shell;
    $wshell.AppActivate('Edge')
    $tfa = bw get totp discord

    if ($env:DISCORD_PW.length -gt 0) {
        # we have discord auth info, do the login
        Start-Sleep -Milliseconds 6000
        $cmd_string = $env:DISCORD_EMAIL + "{TAB}" + $env:DISCORD_PW + "{ENTER}"
        $wshell.SendKeys((Set-EscapeCharactersSendKeys ($env:DISCORD_EMAIL + "{TAB}")))
        Start-Sleep -Milliseconds 700
        $wshell.SendKeys((Set-EscapeCharactersSendKeys ($env:DISCORD_PW + "{ENTER}")))
        Start-Sleep -Milliseconds 1300
        $wshell.SendKeys($tfa + "{ENTER}")
    }
}

