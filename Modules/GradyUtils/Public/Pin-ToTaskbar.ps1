function Pin-ToTaskbar ([string] $name, [string] $exePath) {
    $taskbar_path = Join-Path $env:appdata "Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar"
    Write-Output $taskbar_path
    $WshShell = New-Object -comObject WScript.Shell

    $shortcut_path = Join-Path $taskbar_path ($name + ".lnk")
    Write-Output $shortcut_path
    $Shortcut = $WshShell.CreateShortcut($shortcut_path)
    $Shortcut.TargetPath = $exePath
    $Shortcut.save()
}
