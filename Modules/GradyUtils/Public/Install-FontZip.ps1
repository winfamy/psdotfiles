function Install-FontZip ([System.IO.FileInfo] $zipFile) {
    # expand zip    
    $destPath = $zipFile.PSParentPath + "\" + $zipFile.Basename;
    Expand-Archive -LiteralPath $zipFile.PSPath -Destination $destPath;

    # get the list of files to copy
    $fontFiles = gci $destPath;
    $fontFinalPath = $env:LOCALAPPDATA + "\Microsoft\Windows\Fonts";
    
    # copy all font-items to finalPath
    $fontFiles = Get-ChildItem -Path ($destPath + "\*") -Include *.ttf,*.otf;
    foreach ($fontFile in $fontFiles) {
        $keyPath = "HKCU:\Software\Microsoft\Windows NT\CurrentVersion\Fonts";
        New-ItemProperty -Name $fontFile.Basename -Path $keyPath -PropertyType string -Value $fontFile -Force > $null;
    }

    Log-Success ("Installed font " + $zipFile.Basename);
}
