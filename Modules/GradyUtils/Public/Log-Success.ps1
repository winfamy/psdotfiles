function Log-Success ([string] $message) {
    Write-ColorOutput "[" White Black -NoNewLine
    Write-ColorOutput "+" Green Black -NoNewLine
    Write-ColorOutput "] " White Black -NoNewLine
    Write-ColorOutput $message
}
