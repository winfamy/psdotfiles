function Install-Fonts ([string] $fontPath) {
    $fontZips = Get-ChildItem -Path ($fontPath + "\*") -Include *.zip;
    foreach ($fontZip in $fontZips) {
        Install-FontZip $fontZip;
    }
}
