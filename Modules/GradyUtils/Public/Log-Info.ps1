function Log-Info ([string] $message) {
    Write-ColorOutput "[" White Black -NoNewLine
    Write-ColorOutput " " White Black -NoNewLine
    Write-ColorOutput "] " White Black -NoNewLine
    Write-ColorOutput $message
}

