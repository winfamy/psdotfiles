function BW-Relogin() {
        # load bitwarden stuff
        if ($env:BW_EMAIL.Length -eq 0) {
            ## load bitwarden email
            $email = Read-Host "Bitwarden Email" 
            [Environment]::SetEnvironmentVariable("BW_EMAIL", $email, "User")
            $env:BW_EMAIL = $email
        }

        if ($env:BW_PW.Length -eq 0) {
            ## load and decrypt bitwarden password
            $pwd = Read-Host "Bitwarden Master Password" -AsSecureString
            $pwd_plaintext = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($pwd))
            [Environment]::SetEnvironmentVariable("BW_PW", $pwd_plaintext, "User")
            $env:BW_PW = $pwd_plaintext
        }

        # login to BW immediately if needed
        $bwLoginCheck = (& bw login --check) -eq "You are logged in!"
        if (-not $bwLoginCheck) {
            $null = & bw login $env:BW_EMAIL --passwordenv "BW_PW" | out-null
        }

        $env:BW_SESSION = & bw unlock --passwordenv "BW_PW" --raw
        [Environment]::SetEnvironmentVariable("BW_SESSION", $env:BW_SESSION, "User")

        if ($env:BW_SESSION.Length -eq 0) {
            Log-Error "Failed Bitwarden login!"
        } else {
            Log-Success "Logged into Bitwarden!"
        }
}
