function Log-Alert ([string] $message) {
    Write-ColorOutput "[" White Black -NoNewLine
    Write-ColorOutput "!" Yellow Black -NoNewLine
    Write-ColorOutput "] " White Black -NoNewLine
    Write-ColorOutput $message
}

