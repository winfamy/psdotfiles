Function Disable-ProxyScript() {
    Log-Success "Disabled proxy script"
    set-itemproperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -Name AutoConfigUrl -Value ''
}
