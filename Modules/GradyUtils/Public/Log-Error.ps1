function Log-Error ([string] $message) {
    Write-ColorOutput "[" White Black -NoNewLine
    Write-ColorOutput "-" Red Black -NoNewLine
    Write-ColorOutput "] " White Black -NoNewLine
    Write-ColorOutput $message
}
