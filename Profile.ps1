# Author: 2d Lt Grady Phillips
# Grady's Powershell Profile

# import modules from local module lib
Import-Module GradyUtils -DisableNameChecking

# environment stuff
$ErrorActionPreference= 'silentlycontinue'
# this allows bitwarden to connect via API even tho SSL connections dont work right
$env:NODE_TLS_REJECT_UNAUTHORIZED = 0
$env:NODE_NO_WARNINGS = 1
$env:MYVIMRC="$HOME\.vimrc"
$env:BW_SESSION = [Environment]::GetEnvironmentVariable("BW_SESSION", "User")
$env:DISCORD_PW = [Environment]::GetEnvironmentVariable("DISCORD_PW", "User")
$env:DISCORD_EMAIL = [Environment]::GetEnvironmentVariable("DISCORD_EMAIL", "User")

# aliases
Set-Alias -Name disc -Value Launch-Discord
Set-Alias -Name md -Value Show-Markdown -Option AllScope

Disable-ProxyScript
