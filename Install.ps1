param ([switch] $Full)
$FullInstall = $Full.isPresent

# disable windows animations
set-itemproperty 'HKCU:\Control Panel\Desktop\WindowMetrics' -Name MinAnimate -Value 0

# build local modules dir if not exist
$localPSModulePath = ($env:PSModulePath -Split ";")[0]
# assigning to $null silences the output
$null = New-Item -ItemType Directory -Force -Path $localPSModulePath

# build path to copy modules directory
$modulesPath = [IO.Path]::Combine($PSScriptRoot, "Modules")
$modulesStar = -join ($modulesPath, "\*");
Copy-Item -Force -Recurse $modulesStar $localPSModulePath
Copy-Item -Force ([IO.Path]::Combine($PSScriptRoot, "Profile.ps1")) $profile
Import-Module GradyUtils -Force -DisableNameChecking
Log-Success "Installed custom modules"
Log-Success "Installed profile"

# copy actual dotfiles
$dotfiles = -join ([IO.Path]::Combine($PSScriptRoot, "Dotfiles"), "\*")
Copy-Item -Force -Recurse $dotfiles $HOME
Log-Success "Installed actual dotfiles"

# import modules from local module lib
Import-Module EnvPathFunctions
$zips_path = Join-Path $PSScriptRoot "\Path\*.zip"
$zips = gci $zips_path

# create this now so we can add the directories from unzipping to $PATH
$pathsToAddToPath = @(
    Join-Path $PSScriptRoot "\Path"
)

if ($FullInstall) {
    foreach ($zip in $zips) {
        $new_path = [IO.Path]::Combine($PSScriptRoot, "Path", $zip.Basename)
        if (-not (test-path $new_path)) {
            Log-Success ("Extracting " + $zip.Basename)
            Expand-Archive -LiteralPath $zip.PSPath -DestinationPath $new_path -Force *>$null
        } else {
            Log-Alert ($zip.Basename + " already installed.")
        }

        $pathsToAddToPath += $new_path
    }
    
    Log-Success "Installing fonts."
    $zipPath = Join-Path $PSScriptRoot "Fonts"
    Install-Fonts $zipPath
}

foreach ($path in $pathsToAddToPath) {
    if ( !(Get-DoesExistInEnvPath $path) ) {
        Log-Success "$path added to PATH"
        Add-EnvPath $path
    }
}

if ($FullInstall) {
    $bwLoginCheck = (& bw login --check) -eq "You are logged in!"
    if (-not $bwLoginCheck) {
        Log-Info "Not logged in to Bitwarden, getting user info..."

        # load bitwarden stuff
        ## load bitwarden email
        $email = Read-Host "Bitwarden Email" 
        [Environment]::SetEnvironmentVariable("BW_EMAIL", $email, "User")
        $env:BW_EMAIL = $email

        ## load and decrypt bitwarden password
        $pwd = Read-Host "Bitwarden Master Password" -AsSecureString
        $pwd_plaintext = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($pwd))
        [Environment]::SetEnvironmentVariable("BW_PW", $pwd_plaintext, "User")
        $env:BW_PW = $pwd_plaintext

        # login to BW immediately if needed
        $null = & bw login $env:BW_EMAIL --passwordenv "BW_PW" 
        $env:BW_SESSION = & bw unlock --passwordenv "BW_PW" --raw
        [Environment]::SetEnvironmentVariable("BW_SESSION", $env:BW_SESSION, "User")

        if ($env:BW_SESSION.Length -eq 0) {
            Log-Error "Failed Bitwarden login!"
        } else {
            Log-Success "Logged into Bitwarden!"
        }

    } else {
        Log-Alert "Already logged in to Bitwarden!"
    }
    
    $discord_email = Read-Host "Discord Email"
    $env:DISCORD_EMAIL = $discord_email
    [Environment]::SetEnvironmentVariable("DISCORD_EMAIL", $discord_email, "User")
    
    ## load and decrypt discord password
    $discord_pwd = Read-Host "Discord Password" -AsSecureString
    $discord_pwd_plaintext = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($discord_pwd))
    $env:DISCORD_PW = $discord_pwd_plaintext
    [Environment]::SetEnvironmentVariable("DISCORD_PW", $env:DISCORD_PW, "User")

    # run installers
    # this happens last because Git bash needs windows terminal to be recognized first before
    # it can add the profile
	Log-Success "Running installers..."
	$installers = gci (Join-Path $PSScriptRoot "Installers")
	foreach ($installer in $installers) {
		start $installer.PSPath
	}
}
