set nocompatible
set runtimepath+=$HOME/vimfiles

set relativenumber
set incsearch
set autoindent
set ruler
set autowrite
set smarttab
set linebreak
set et
set title

set listchars=eol:↵,tab:.\ ,trail:·,extends:→,precedes:←
set mouse=v
set history=50
set tabstop=4
set matchtime=2
set matchpairs+=<:>
set sw=4
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set backspace=2
set encoding=UTF-8
set background=dark
set conceallevel=3
set wildignore+=*.dll,*.exe,*.o,*.obj,*.svn,*.swp,*.class,*.hg,*.git,*.orig,*.DS_Store,*.zip

filetype plugin indent on
filetype indent on
syntax on


" aesthetic options
let g:airline_theme='molokai'
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" powerline symbols
let g:airline#extensions#tabline#enabled = 1 " Enable the list of buffers
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.dirty='⚡'

" devicons options
let g:webdevicons_conceal_nerdtree_brackets = 1
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:WebDevIconsNerdTreeAfterGlyphPadding = ' '
let g:WebDevIconsNerdTreeBeforeGlyphPadding = ''

let NERDTreeShowHidden = 1
let NERDTreeRespectWildIgnore = 1
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_user_command =  
    \ ['.git', 'cd %s && git ls-files -co --exclude-standard'] 
let g:ctrlp_working_path_mode = 'rw'

let g:asyncomplete_auto_popup = 1

call plug#begin('~/.vim/plugged')
    Plug 'preservim/nerdtree'
    Plug 'ryanoasis/vim-devicons'
    Plug 'junegunn/vim-easy-align'
    Plug 'vim-airline/vim-airline'
    Plug 'tpope/vim-surround'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'chriskempson/base16-vim'
    Plug 'joshdick/onedark.vim'
    Plug 'sheerun/vim-polyglot'
    Plug 'flazz/vim-colorschemes'
    Plug 'kien/rainbow_parentheses.vim'
    Plug 'prabirshrestha/asyncomplete.vim'
    Plug 'kien/ctrlp.vim'
call plug#end()

" Preserve scroll position when switching between buffers
au BufLeave * if !&diff | let b:winview = winsaveview() | endif
au BufEnter * if exists('b:winview') && !&diff | call winrestview(b:winview) | unlet! b:winview | endif

" preserve cursor position when leaving file
au BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") && &filetype != "gitcommit" |
        \ execute("normal `\"") |
    \ endif

" Nerdtree scripts
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') |
    \ execute 'NERDTree' argv()[0] | wincmd p | enew | execute 'cd '.argv()[0] | endif
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
autocmd BufWinEnter * if getcmdwintype() == '' | silent NERDTreeMirror | endif
autocmd BufEnter * if winnr() == winnr('h') && bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
    \ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif

" auto-complete windows closes upon autocomplete
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif


" Rainbow parentheses always enabled
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
" au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

nnoremap <Leader>] `[V`]>
nnoremap <Leader>[ `[V`]<
nnoremap <Leader>n :bnext<CR>
nnoremap <Leader>p :bprevious<CR>
nnoremap <Leader>d :bp<bar>bd#<CR> 
nnoremap <Leader>f :CtrlP<CR>
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"

if has('termguicolors')
    " Turns on true terminal colors
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

    " Turns on 24-bit RGB color support
    set termguicolors

    " Defines how many colors should be used. (maximum: 256, minimum: 0)
    set t_Co=256
endif

colorscheme molokai

if exists("g:loaded_webdevicons")
    call webdevicons#refresh()
endif
